<?php

namespace app\models;
use common\models\User; 
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $role_id
 */
class MyUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */




    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'role_id' => 'Role ID',
        ];
    }
    public function signup()
    {
        if ($this->validate())
        {
            $user = new User();
            $user->username = $this->username;
            // $user->email = $this->email;
            $user->setPassword($this->password_hash);
            //$user->generateAuthKey();
            if ($user->save())
            {
                return $user;
            }
        }
        return null;
    }

    public function beforeSave($insert)
    {
        if (!$this->isNewRecord)
        {
            $command = static::getDb()->createCommand("SELECT password_hash FROM \"user\" where id =$this->id")->queryOne();
            if ($command != $this->password_hash)
            {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
            }
        }
        return parent::beforeSave($insert);
    }
    
        /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
        /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
